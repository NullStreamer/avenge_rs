## Running on clean machine
- Install Git using OS's package manager, e.g. for Ubuntu
```sh
sudo apt install git
```

for FreeBSD
```sh
sudo pkg install git
```


- Install Rust using [Rustup](https://rustup.rs), defaults are fine

- Clone the repository
```sh
git clone https://gitlab.com/NullStreamer/avenge_rs
```

- Build and run the application
```sh
cd avenge_rs
cargo run --release -- -t 10000 -p
```

- You can play around with number of concurrent tasks (`-t`) to manage load on machine and network
- `-p` (connect through proxy) can be omitted if application is run on VPS/VDS outside of Ukraine
- Use `git pull` to ckeck for updates once in a while

To quit press "q"


### See also
- [tmux cheatsheet](https://tmuxcheatsheet.com)
