use std::time::Duration;

#[derive(Debug)]
pub(super) struct ReadableDuration {
    value: Duration,
}

impl ReadableDuration {
    pub fn new(value: Duration) -> Self {
        Self { value }
    }
}

impl std::fmt::Display for ReadableDuration {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        const SECONDS_PER_MINUTE: u64 = 60;
        const SECONDS_PER_HOUR: u64 = SECONDS_PER_MINUTE * 60;
        const SECONDS_PER_DAY: u64 = SECONDS_PER_HOUR * 24;
        const SECONDS_PER_WEEK: u64 = SECONDS_PER_DAY * 7;

        struct Entry {
            seconds: u64,
            unit: char,
        }

        const TABLE: [Entry; 4] = [
            Entry {
                seconds: SECONDS_PER_WEEK,
                unit: 'w',
            },
            Entry {
                seconds: SECONDS_PER_DAY,
                unit: 'd',
            },
            Entry {
                seconds: SECONDS_PER_HOUR,
                unit: 'h',
            },
            Entry {
                seconds: SECONDS_PER_MINUTE,
                unit: 'm',
            },
        ];

        let mut force = false;
        let mut x = self.value.as_secs();

        for entry in &TABLE {
            let v = x / entry.seconds;
            x %= entry.seconds;
            if v > 0 || force {
                force = true;
                write!(f, "{}{}", v, entry.unit)?;
            }
        }

        write!(f, "{}s", x)
    }
}
