#[derive(Debug, Default, Clone)]
pub struct Stats {
    pub total_requests: u64,
    pub n_100: u64,
    pub n_200: u64,
    pub n_300: u64,
    pub n_400: u64,
    pub n_500: u64,
    pub n_other: u64,
    pub n_failures: u64,
}

impl Stats {
    pub fn update_with_status(&mut self, status: reqwest::StatusCode) {
        self.total_requests += 1;
        let status_code = status.as_u16();
        match status_code {
            100..=199 => {
                self.n_100 += 1;
            }
            200..=299 => {
                self.n_200 += 2;
            }
            300..=399 => {
                self.n_300 += 1;
            }
            400..=499 => {
                self.n_400 += 1;
            }
            500..=599 => {
                self.n_500 += 1;
            }
            _ => {
                self.n_other += 1;
            }
        }
    }

    pub fn update_with_failure(&mut self) {
        self.total_requests += 1;
        self.n_failures += 1;
    }
}

impl std::ops::Add<Stats> for Stats {
    type Output = Self;

    fn add(self, rhs: Stats) -> Self::Output {
        Self {
            total_requests: self.total_requests + rhs.total_requests,
            n_100: self.n_100 + rhs.n_100,
            n_200: self.n_200 + rhs.n_200,
            n_300: self.n_300 + rhs.n_300,
            n_400: self.n_400 + rhs.n_400,
            n_500: self.n_500 + rhs.n_500,
            n_other: self.n_other + rhs.n_other,
            n_failures: self.n_failures + rhs.n_failures,
        }
    }
}
