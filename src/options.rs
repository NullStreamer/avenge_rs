use structopt::StructOpt;

const fn default_n_tasks() -> &'static str {
    if cfg!(debug_assertions) {
        "5"
    } else {
        "200"
    }
}

#[derive(Debug, StructOpt)]
pub(super) struct Options {
    #[structopt(long, short = "p")]
    pub use_proxy: bool,

    #[structopt(long, short = "l")]
    pub target_list_url: Option<String>,

    #[structopt(long, short = "u", default_value = "15")]
    pub target_update_interval_min: u32,

    #[structopt(long, short = "r")]
    pub proxy_list_url: Option<String>,

    #[structopt(long, short = "t", default_value = default_n_tasks())]
    pub n_tasks: std::num::NonZeroUsize,

    #[structopt(long, short = "T", default_value = "10")]
    pub conn_timeout_sec: u64,
}
