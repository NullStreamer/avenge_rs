use crossterm::{event, terminal};

// A sort of RAII wrapper for TUI initialization/cleanup
// Technically it can fail during cleanup and leave user's terminal in an unusable state
pub(super) struct TuiHelper {
    pub terminal: tui::Terminal<tui::backend::CrosstermBackend<std::io::Stdout>>,
}

impl TuiHelper {
    pub fn new() -> Result<Self, std::io::Error> {
        terminal::enable_raw_mode()?;
        let mut stdout = std::io::stdout();
        crossterm::execute!(
            stdout,
            terminal::EnterAlternateScreen,
            event::EnableMouseCapture
        )?;
        let backend = tui::backend::CrosstermBackend::new(stdout);
        let terminal = tui::Terminal::new(backend)?;
        Ok(Self { terminal })
    }

    fn drop(&mut self) -> Result<(), std::io::Error> {
        terminal::disable_raw_mode()?;
        crossterm::execute!(
            self.terminal.backend_mut(),
            terminal::LeaveAlternateScreen,
            event::DisableMouseCapture
        )?;
        self.terminal.show_cursor()?;

        Ok(())
    }
}

impl Drop for TuiHelper {
    fn drop(&mut self) {
        Self::drop(self).expect("Error during TUI deinitialization");
    }
}