use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub(super) struct Target {
    #[serde(rename = "page")]
    pub url: String,
}
